                                                          
                                                                 CI/CD 

CI/CD stands for Continuous integration (CI) and continuous delivery (CD).

Continuous Integration:

Continuous integration is a coding philosophy and set of practices that drive development teams to implement small changes and check in code to version control repositories frequently. Because most modern applications require developing code in different platforms and tools, the team needs a mechanism to integrate and validate its changes.

Continous Delivery:

Continuous delivery picks up where continuous integration ends. CD automates the delivery of applications to selected infrastructure environments. Most teams work with multiple environments other than the production, such as development and testing environments, and CD ensures there is an automated way to push code changes to them.  CD automation then performs any necessary service calls to web servers, databases, and other services that may need to be restarted or follow other procedures when applications are deployed.


Tools for CI/CD:

1) JENKINS
2) Circle CI
3) TeamCity
4) GitLab
5) Travis CI
6) Codeship
7) GoCD
8) Wercker
9) Semaphore
10) Nevercode
11) Spinnaker
12) Buildbot
