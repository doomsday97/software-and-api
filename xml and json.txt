                                           JSON AND XML

JSON:  

It is based on the JavaScript programming language and easy to understand and generate.
Its an open-standard file format that is used for browser-server communications. It’s a language independent data format.
It is derived from javascript.
It is used to transmit the data in a passable manner via internet.
It is less verbose and faster.
Its data structure is a map. Map is similar to key-value pairs.
JSON is preferred for data delivery between servers and browsers.
JSON supports array and only supports text and number datatypes.

BASIC SYNTAX:
{
  "artists" : [
    { "artistname" : "Deep Purple", "formed" : "1968" },
    { "artistname" : "Joe Satriani", "born" : "1956" },
    { "artistname" : "Maroon 5", "formed" : "1994" }
  ]
}



XML: 

It’s a set of rules that help the users to encode the documents in a human-readable format and machine-readable. 
XML uses more words to describe the intention. Parsing XML software is slow and tedious job.
it is derived from SGML (Standard Generalized Markup Language).
It uses tree data structure. This means working on XML a tedious and time-consuming task. 
It is preferred for storing information on the server side.
XML is document oriented.
XML provide display properties as it’s an markup language.
Xml has variety of data types like text, numbers, images, graphs etc.
  
BASIC SYNTAX:
<root>
  <child>Data</child>
  <child>More Data</child>
</root>




                          SOAP and REST

SOAP:
The main idea behind designing SOAP was to ensure that programs built on different platforms and programming languages could exchange data in an easy manner.
SOAP can only work with XML format. All the data passed is in XML format.
SOAP is used when we need a proper information flow between browser to server. 
SOAP uses service interfaces to expose its functionality to client applications.
SOAP needs more bandwidth for its usage.
It can be used in a variety of messaging systems and can be delivered via a variety of transport protocols, the initial focus of SOAP is remote procedure calls transported via HTTP.


REST:
It was designed specially for working with components such as media components, files.
A Restful service would use the normal HTTP verbs of GET, POST, PUT and DELETE for working with the required components. 
REST can make use of SOAP as the underlying protocol for web services, because in the end it is just an architectural pattern.
REST permits different data format such as Plain text, HTML, XML, JSON, etc. But the most preferred format for transferring data is JSON.